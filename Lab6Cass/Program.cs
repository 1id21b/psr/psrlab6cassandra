﻿using Cassandra;
using Cassandra.Mapping;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using Cassandra.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6Cass.Repository;

namespace Lab6Cass
{
    class Program
    {
        static void Main(string[] args)
        {

            var cluster = Cluster.Builder().AddContactPoints("localhost").Build();
            //session.Execute("CREATE KEYSPACE Library WITH replication " + "= {'class':'SimpleStrategy', 'replication_factor':1}; ");
            var session = cluster.Connect("library");
            Initalize init = new Initalize(session);
            init.CreateOrResetTable();
            MappingConfiguration.Global.Define<MyMappings>();
            init.InsertRows();

            string menu = "";
            while (menu != "q")
            {
                MainMenu();
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        BookMenu(session);
                        continue;
                    case "2":
                        UserMenu(session);
                        continue;
                    case "3":
                        RentMenu(session);
                        continue;
                }
            }

        }

        static void MainMenu()
        {
            Console.WriteLine("Menu glowne:");
            Console.WriteLine("1. Menu Ksiazki");
            Console.WriteLine("2. Menu Uzytkownika");
            Console.WriteLine("3. Menu Wypozyczenia");
            Console.WriteLine("q. Wyjście");
        }
        static void BookMenu(ISession sess)
        {
            BookRepo repo = new BookRepo(sess);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu ksiazki:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl dostępne ksiązki");
                Console.WriteLine("3. Wyswietl pojedyńczy");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Book b = new Book();
                        Console.WriteLine("Podaj Tytuł");
                        b.Title = Console.ReadLine();
                        Console.WriteLine("Podaj Opis");
                        b.Description = Console.ReadLine();
                        repo.AddBook(b);
                        continue;
                    case "2":
                        repo.ShowBooks();
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id ksiazki");
                        string id = Console.ReadLine();
                        repo.ShowBook(id);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id ksiazki");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj nowy tytul ksiazki");
                        string title = Console.ReadLine();
                        Console.WriteLine("Podaj nowy opis");
                        string description = Console.ReadLine();
                        Book bk = new Book(Guid.Parse(id), title, description, Guid.Empty);
                        repo.EditBook(bk);
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id ksiazki");
                        id = Console.ReadLine();
                        repo.DeleteBook(id);
                        continue;
                }
            }
        }
        static void UserMenu(ISession session)
        {
            UserRepo repo = new UserRepo(session);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu uzytkownika:");
                Console.WriteLine("1. Dodaj");
                Console.WriteLine("2. Wyswietl");
                Console.WriteLine("3. Wyswietl pojedyńczego");
                Console.WriteLine("4. Edytuj");
                Console.WriteLine("5. Usun");
                Console.WriteLine("6. Wyswietl ksiazki uzytkownika");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        User u = new User();
                        Console.WriteLine("Podaj imie uzytkownika");
                        u.Name = Console.ReadLine();
                        Console.WriteLine("Podaj nazwisko uzytkownika");
                        u.Surname = Console.ReadLine();
                        repo.AddUser(u);
                        continue;
                    case "2":
                        repo.ShowUsers();
                        continue;
                    case "3":
                        Console.WriteLine("Podaj id uzytkownika");
                        string id = Console.ReadLine();
                        repo.ShowUser(id);
                        continue;
                    case "4":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();
                        Console.WriteLine("Podaj nowe imie uzytkownika");
                        string name = Console.ReadLine();
                        Console.WriteLine("Podaj nowe nazwisko uzytkownika");
                        string surname = Console.ReadLine();
                        u = new User(Guid.Parse(id), name, surname);
                        repo.EditUser(u);
                        continue;
                    case "5":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();
                        repo.DeleteUser(id);
                        continue;
                    case "6":
                        Console.WriteLine("Podaj id uzytkownika");
                        id = Console.ReadLine();
                        repo.ShowUserBooks(id);
                        continue;
                }
            }
        }
        static void RentMenu(ISession session)
        {
            RentalRepo repo = new RentalRepo(session);
            string menu = "";
            while (menu != "q")
            {
                Console.WriteLine("Menu Wypozyczenia:");
                Console.WriteLine("1. Wypozycz");
                Console.WriteLine("2. Oddaj");
                Console.WriteLine("q. Wyjscie");
                menu = Console.ReadLine();
                switch (menu)
                {
                    case "1":
                        Console.WriteLine("Podaj id uzytkownika");
                        string u = Console.ReadLine();
                        repo.Rent(u);
                        continue;
                    case "2":
                        Console.WriteLine("Podaj id ksiazki");
                        u = Console.ReadLine();
                        repo.GiveBack(u);
                        continue;
                }
            }
        }
    }
}
