﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Model
{
    class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname {get;set;}

        public User()
        {
            Id = Guid.NewGuid();
        }

        public User(string name, string surname)
        {
            Id = Guid.NewGuid();
            Name = name;
            Surname = surname;
        }

        public User(Guid id, string name, string surname)
        {
            Id = id;
            Name = name;
            Surname = surname;
        }

        public override string ToString()
        {
            return "Uzytkownik - Id: " + Id + " Imie: " + Name + " Nazwisko: " + Surname;
        }
    }
}
