﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Model
{
    class Book
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid User_Id{ get; set; }

        public Book()
        {
            Id = Guid.NewGuid();
        }

        public Book(string title, string description, Guid u_id)
        {
            Id = Guid.NewGuid();
            Title = title;
            Description = description;
            User_Id = u_id;
        }

        public Book(Guid id, string title, string description, Guid u_id)
        {
            Id = id;
            Title = title;
            Description = description;
            User_Id = u_id;
        }

        public override string ToString()
        {
            return "Ksiazka - Id: " + Id + " Tytul: " + Title + " Opis: " + Description + (User_Id == new Guid()? " Niewypozyczona" : " Wypozyczona");
        }
    }
}
