﻿using Cassandra.Mapping;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass
{
    class MyMappings : Mappings
    {

        public MyMappings()
        {
            For<Book>()
                .TableName("books")
                .PartitionKey(b => b.Id)
                .Column(b => b.Id, cm => cm.WithName("Id"));
            For<User>()
                .TableName("users");
                //.Column(s => s.Id, cm => cm.WithName("Id"))
                //.Column(s => s.Name, cm => cm.WithName("Name"));
        }

    }
}
