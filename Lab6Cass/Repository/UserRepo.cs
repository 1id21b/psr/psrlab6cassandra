﻿using Cassandra;
using Cassandra.Data.Linq;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Repository
{

    class UserRepo : RepoBase
    {
        Table<User> users { get; set; }
        public UserRepo(ISession session) : base(session)
        {
            users = new Table<User>(session);
        }

        public void AddUser(User User)
        {
            users.Insert(User).Execute();
            Console.WriteLine("Dodano " + User);
        }

        public void DeleteUser(string id)
        {
            Guid _id = Guid.Parse(id);
            User User = users.Where(b => b.Id == _id).First().Execute();
            users.Where(b => b.Id == _id).Delete().Execute();
            Console.WriteLine("Usunieto " + User);
        }

        public void EditUser(User _User)
        {
            users.Where(b => b.Id == _User.Id).Select(b => new User(_User.Name, _User.Surname)).Update().Execute();
            Console.WriteLine("Edytowano: " + _User);
        }

        public void ShowUsers()
        {
            IEnumerable<User> allUsers = users.Execute();
            foreach (User b in allUsers)
            {
                Console.WriteLine(b);
            }
        }

        public void ShowUser(string id)
        {
            Guid _id = Guid.Parse(id);
            User user = users.Where(b => b.Id == _id).First().Execute();
            Console.WriteLine(user);
        }

        public void ShowUserBooks(string id)
        {
            Guid _id = Guid.Parse(id);
            User user = users.Where(b => b.Id == _id).First().Execute();
            Console.WriteLine(user);
            Table<Book> books = new Table<Book>(_session);
            IEnumerable<Book> rentedBooks = books.Where(b => b.User_Id == user.Id).AllowFiltering().Execute();
            foreach(var b in rentedBooks)
            {
                Console.WriteLine(b);
            }
        }
    }
}
