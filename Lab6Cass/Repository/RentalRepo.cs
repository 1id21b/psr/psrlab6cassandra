﻿using Cassandra;
using Cassandra.Data.Linq;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Repository
{
    class RentalRepo : RepoBase
    {
        Table<User> users { get; set; }
        Table<Book> books { get; set; }
        public RentalRepo(ISession session) : base(session)
        {
            books = new Table<Book>(session);
            users = new Table<User>(session);
        }

        public void Rent(string User_id)
        {
            IEnumerable<Book> allBooks = books.Execute();
            IEnumerable<Book> notRentedBooks = allBooks.Where(b => b.User_Id == Guid.Empty).ToList();
            foreach (Book b in notRentedBooks)
            {
                Console.WriteLine(b);
            }
            Console.WriteLine("Podaj id ksiazki do wypozyczenia");
            string s = Console.ReadLine();

            Book _book = books.Where(b => b.Id == Guid.Parse(s)).First().Execute();
            _book.User_Id = Guid.Parse(User_id);

            books.Where(b => b.Id == _book.Id).Select(b => new Book(_book.Title, _book.Description, _book.User_Id)).Update().Execute();
            Console.WriteLine("Wypozyczono: " + _book);
        }
        public void GiveBack(string Book_Id)
        {
            Guid uId = Guid.Parse(Book_Id);
            Book _book = books.Where(b => b.Id == uId).First().Execute();
            _book.User_Id = Guid.Empty;
            books.Where(b => b.Id == uId).Select(b => new Book(_book.Title, _book.Description, _book.User_Id)).Update().Execute();
            Console.WriteLine("Oddano: " + _book);
        }
    }
}
