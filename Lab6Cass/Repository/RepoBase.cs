﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Repository
{
    public class RepoBase
    {
        protected ISession _session { get; set; }

        public RepoBase()
        {
        }

        public RepoBase(ISession session)
        {
            _session = session;
        }
    }
}
