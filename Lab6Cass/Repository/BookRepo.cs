﻿using Cassandra;
using Cassandra.Data.Linq;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass.Repository
{

    class BookRepo : RepoBase
    {
        Table<Book> books { get; set; }
        public BookRepo(ISession session) : base(session)
        {
            books = new Table<Book>(session);
        }

        public void AddBook(Book book)
        {
            books.Insert(book).Execute();
            Console.WriteLine("Dodano " + book);
        }

        public void DeleteBook(string id)
        {
            Guid _id = Guid.Parse(id);
            Book book = books.Where(b => b.Id == _id).First().Execute();
            books.Where(b => b.Id == _id).Delete().Execute();
            Console.WriteLine("Usunieto " + book);
        }

        public void EditBook(Book _book)
        {
            books.Where(b => b.Id == _book.Id).Select(b => new Book(_book.Title, _book.Description, _book.User_Id)).Update().Execute();
            Console.WriteLine("Zedytowano: " + _book);
        }

        public void ShowBooks()
        {
            Guid id = Guid.Empty;
            IEnumerable<Book> allBooks = books.Execute();
            IEnumerable<Book> notRentedBooks = allBooks.Where(b => b.User_Id == Guid.Empty).ToList();
            foreach (Book b in notRentedBooks)
            {
                Console.WriteLine(b);
            }
        }

        public void ShowBook(string id)
        {
            Guid _id = Guid.Parse(id);
            Book book = books.Where(b => b.Id == _id).First().Execute();
            Console.WriteLine(book);
        }
    }
}
