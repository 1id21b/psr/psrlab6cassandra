﻿using Cassandra;
using Cassandra.Data.Linq;
using Lab6Cass.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Cass
{
    class Initalize
    {
        private ISession session { get; set; }

        public Initalize(ISession session)
        {
            this.session = session;
        }

        public void CreateOrResetTable()
        {
            session.Execute("drop table books");
            session.Execute("drop table users");
            session.Execute("create Table books (Id uuid, Title text, Description text, User_Id uuid, PRIMARY KEY (Id));");
            session.Execute("create Table users (Id uuid, Name text, Surname text, PRIMARY KEY (Id))");

        }

        public void InsertRows()
        {
            var books = new Table<Book>(session);
            var users = new Table<User>(session);
   
            long count = users.Count().Execute();

            if (count == 0)
            {
                users.Insert(new User("Jan", "Kowalski")).Execute();
                users.Insert(new User("Stefan", "Nowak")).Execute();
                users.Insert(new User("Norbert", "Kaczka")).Execute();
                users.Insert(new User("Maciej", "Metal")).Execute();
                users.Insert(new User("Ksawery", "Pierogi")).Execute();
            }
            count = books.Count().Execute();

            if (count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    books.Insert(new Book("Ksiazka" + i, "Opis" + i, Guid.Empty)).Execute();
                }
            }
        }
    }
}
